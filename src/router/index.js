/*
 * @Author: your name
 * @Date: 2020-03-03 11:22:48
 * @LastEditTime: 2020-12-08 15:38:49
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \yuanzhou-salesH5\src\router\index.js
 */
import { createBrowserHistory } from 'history';
import MainLayout from '@/components/Layouts';
import LoadableComponent from '@/components/LoadableComponent'

const Index = LoadableComponent(() => import('@/pages/Index'));
const Login = LoadableComponent(() => import('@/pages/Login'));
const ShiJi = LoadableComponent(() => import('@/pages/ShiJi'));

export const history = createBrowserHistory();

export const routesadmin = [
  {
    path:'/',
    redirect:'/index'
  },
  {
    path:'/index',
    layout: MainLayout,
    component: Index
  },
  {
    path:'/shiji',
    component: ShiJi
  }
]

export const routesdefault = [
  {
    path:'/login',
    component: Login
  }
]