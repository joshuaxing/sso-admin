import React, { Component, useState, useEffect } from "react";
import "./style.scss";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from "@/store/actions";
import * as api from "@/api/user";
import { Form, Input, Button, notification, Row, Col } from "antd";
import { randomNum, calculateWidth, preloadingImages } from "@/utils/util";
import BGParticle from "@/utils/BGParticle";
import PromptBox from "@/components/PromptBox";
import Loading from "@/components/Loading";
import bg1 from "./img/bg1.jpg";
import "animate.css";
import { setUserToken } from "@/utils/gotToken";
const url = bg1;

const LoginForm = ({ history }) => {
  const [form] = Form.useForm();
  let canvasObj = null;
  // 聚焦失焦事件
  const [focusItem, setFocusItem] = useState(-1);
  // 设置验证码
  const [checkcode, setCode] = useState("");
  // 生成验证码
  const createCode = () => {
    const ctx = canvasObj.getContext("2d");
    const chars = [
      1,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9,
      "a",
      "b",
      "c",
      "d",
      "e",
      "f",
      "g",
      "h",
      "j",
      "k",
      "l",
      "m",
      "n",
      "p",
      "q",
      "r",
      "s",
      "t",
      "u",
      "v",
      "w",
      "x",
      "y",
      "z",
      "A",
      "B",
      "C",
      "D",
      "E",
      "F",
      "G",
      "H",
      "J",
      "K",
      "L",
      "M",
      "N",
      "P",
      "Q",
      "R",
      "S",
      "T",
      "U",
      "V",
      "W",
      "X",
      "Y",
      "Z",
    ];
    let code = "";
    ctx.clearRect(0, 0, 80, 39);
    for (let i = 0; i < 4; i++) {
      const char = chars[randomNum(0, 57)];
      code += char;
      ctx.font = randomNum(20, 25) + "px SimHei"; //设置字体随机大小
      ctx.fillStyle = "#D3D7F7";
      ctx.textBaseline = "middle";
      ctx.shadowOffsetX = randomNum(-3, 3);
      ctx.shadowOffsetY = randomNum(-3, 3);
      ctx.shadowBlur = randomNum(-3, 3);
      ctx.shadowColor = "rgba(0, 0, 0, 0.3)";
      let x = (80 / 5) * (i + 1);
      let y = 39 / 2;
      let deg = randomNum(-25, 25);
      /**设置旋转角度和坐标原点**/
      ctx.translate(x, y);
      ctx.rotate((deg * Math.PI) / 180);
      ctx.fillText(char, 0, 0);
      /**恢复旋转角度和坐标原点**/
      ctx.rotate((-deg * Math.PI) / 180);
      ctx.translate(-x, -y);
    }
    setCode(code);
  };

  useEffect(() => {
    createCode();
  }, []);

  // 输入框校验
  const [checkName, setCheckName] = useState(false);
  const [checkPass, setCheckPass] = useState(false);
  const [checkCode, setCheckCode] = useState(false);
  const onChangeName = async (param, e) => {
    const value = e.target.value ? false : true;
    form.validateFields([param]).then(
      (result) => {
        console.log('result', result)
        console.log(form.getFieldError(["code"]))
      }
    ).catch(
      (error) => {
        console.log('error', error)
        console.log(form.getFieldError(["code"]))
      }
    )
    try {
      await form.validateFields([param]);
      handleCheck(param, value);
    } catch (errorInfo) {
      handleCheck(param, value);
    }
  };

  const handleCheck = (param, value) => {
    switch (param) {
      case "username":
        setCheckName(value);
        break;
      case "password":
        setCheckPass(value);
        break;
      case "code":
        setCheckCode(value);
        break;
      default:
    }
  };

  const onFinishFailed = (values) => {
    // console.log(values);
    const errorFields = values.errorFields;
    errorFields.forEach((item) => {
      handleCheck(item.name[0], true);
    });
  };

  /*
  function validateCode(value) {
    if (value === "") {
      return {
        validateStatus: "error",
        errorMsg: "请输入验证码",
      };
    } else if (value.toLowerCase() === checkcode.toLowerCase()) {
      return {
        validateStatus: "success",
        errorMsg: null,
      };
    } else {
      return {
        validateStatus: "error",
        errorMsg: "验证码输入错误",
      };
    }
  }

  const [codevalue, setCodeValue] = useState({
    value: "",
  });
  const onChangeCode = (e) => {
    const value = e.target.value;
    setCodeValue({ ...validateCode(value), value });
  };
  */

  const onFinish = (values) => {
    console.log(values);
    const { username, password, code } = values;
    if (code.toLowerCase() !== checkcode.toLowerCase()) {
      setCheckCode(true);
    } else {
      const params = {
        empNo: "",
        password: password,
        phone: username,
      };
      api
        .login({ data: params })
        .then((result) => {
          console.log(result);
          //setUserToken()
        })
        .catch((error) => {
          console.log(error);
        });
    }
  };

  return (
    <div className="box showBox">
      <h3 className="title">管理员登录</h3>
      <Form
        form={form}
        name="form-hooks"
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
      >
        <Form.Item
          name="username"
          rules={[
            {
              required: true,
              message: "请输入用户名",
            },
          ]}
          validateStatus="error"
          // help={
          //   checkName && (
          //     <PromptBox
          //       info={form.getFieldError(["username"])}
          //       width={calculateWidth(form.getFieldError(["username"]))}
          //     />
          //   )
          // }
          // help={'dadadadad'}
        >
          <Input
            onChange={(e) => onChangeName("username", e)}
            onFocus={() => setFocusItem(0)}
            onBlur={() => setFocusItem(-1)}
            maxLength={16}
            placeholder="用户名"
            addonBefore={
              <span
                className="iconfont icon-User"
                style={focusItem === 0 ? styles.focus : {}}
              />
            }
          />
        </Form.Item>
        <Form.Item
          name="password"
          rules={[
            {
              required: true,
              message: "请输入密码",
            },
          ]}
          help={
            checkPass && (
              <PromptBox
                info={form.getFieldError(["password"])}
                width={calculateWidth(form.getFieldError(["password"]))}
              />
            )
          }
        >
          <Input
            onChange={(e) => onChangeName("password", e)}
            onFocus={() => setFocusItem(1)}
            onBlur={() => setFocusItem(-1)}
            maxLength={16}
            placeholder="密码"
            type="password"
            addonBefore={
              <span
                className="iconfont icon-suo1"
                style={focusItem === 1 ? styles.focus : {}}
              />
            }
          />
        </Form.Item>
        <Form.Item
          name="code"
          rules={[
            {
              required: true,
              message: "请输入验证码",
            },
            {
              validator(rule, value) {
                console.log(value)
                if (!value) {
                  return Promise.reject('请输入验证码');
                }
                if (value.toLowerCase() === checkcode.toLowerCase()) {
                  return Promise.resolve();
                }
                return Promise.reject('验证码输入错误');
              }
            }
          ]}
          help={
            checkCode && (
              <PromptBox
                info={form.getFieldError(["code"])}
                width={calculateWidth(form.getFieldError(["code"]))}
              />
            )
          }
          // validateStatus={codevalue.validateStatus}
          // help={
          //   codevalue.errorMsg && (
          //     <PromptBox
          //       info={[codevalue.errorMsg]}
          //       width={calculateWidth([codevalue.errorMsg])}
          //     />
          //   )
          // }
        >
          <Row>
            <Col span={15}>
              <Input
                onChange={(e) => onChangeName("code",e)}
                onFocus={() => setFocusItem(2)}
                onBlur={() => setFocusItem(-1)}
                maxLength={4}
                placeholder="验证码"
                addonBefore={
                  <span
                    className="iconfont icon-securityCode-b"
                    style={focusItem === 2 ? styles.focus : {}}
                  />
                }
              />
            </Col>
            <Col span={9}>
              <canvas
                onClick={createCode}
                width="80"
                height="39"
                ref={(el) => (canvasObj = el)}
              />
            </Col>
          </Row>
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit">
            登录
          </Button>
        </Form.Item>
      </Form>
      <div className="footer">
        <div>欢迎登录后台管理系统</div>
      </div>
    </div>
  );
};

@connect(
  (state) => state.user,
  (dispatch) => bindActionCreators(actions, dispatch)
)
class Login extends Component {
  state = {
    loading: true,
  };

  componentDidMount() {
    this.initPage();
  }

  componentWillUnmount() {
    this.particle && this.particle.destory();
    notification.destroy();
  }

  //载入页面时的一些处理
  initPage = () => {
    this.loadImageAsync(url)
      .then((url) => {
        this.setState(
          {
            loading: false,
          },
          () => {
            this.particle = new BGParticle("backgroundBox");
            this.particle.init();
          }
        );
      })
      .catch((error) => {
        console.log(error);
      });
  };

  //登录的背景图太大，等载入完后再显示，实际上是图片预加载，
  loadImageAsync(url) {
    return new Promise(function (resolve, reject) {
      const image = new Image();
      image.onload = function () {
        resolve(url);
      };
      image.onerror = function () {
        reject("图片载入错误");
      };
      image.src = url;
    });
  }

  render() {
    const { loading } = this.state;
    const props = this.props;
    return (
      <div id="login-page">
        {loading ? (
          <div>
            <h3 style={styles.loadingTitle} className="animated bounceInLeft">
              载入中...
            </h3>
            <Loading />
          </div>
        ) : (
          <div>
            <div id="backgroundBox" style={styles.backgroundBox}></div>
            <div className="container">
              <LoginForm history={props.history} />
            </div>
          </div>
        )}
      </div>
    );
  }
}

const styles = {
  backgroundBox: {
    position: "fixed",
    top: "0",
    left: "0",
    width: "100vw",
    height: "100vh",
    backgroundImage: `url(${require("./img/bg1.jpg")})`,
    backgroundSize: "100% 100%",
    transition: "all .5s",
  },
  focus: {
    width: "20px",
    opacity: 1,
  },
  loadingBox: {
    position: "fixed",
    top: "50%",
    left: "50%",
    transform: "translate(-50%,-50%)",
  },
  loadingTitle: {
    position: "fixed",
    top: "50%",
    left: "50%",
    marginLeft: -45,
    marginTop: -18,
    color: "#000",
    fontWeight: 500,
    fontSize: 24,
  },
};

export default Login;
