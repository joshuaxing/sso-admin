/*
 * @Author: your name
 * @Date: 2020-11-23 11:20:01
 * @LastEditTime: 2020-12-31 12:47:05
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \sso-admin\src\pages\ShiJi\index.js
 */

import React, { Component } from "react";
import { Card, Button, message } from "antd";
import "./style.scss";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from "@/store/actions";
import * as api from "@/api/user";
@connect(
  (state) => state.user,
  (dispatch) => bindActionCreators(actions, dispatch)
)
class ShiJi extends Component {
  constructor(props) {
    super(props);
    this.iframe = React.createRef();
  }
  iframeloadcount = 0;
  state = {
    systemno: "",
    password: "",
    code: "",
    shijisrc: "",
  };

  componentDidMount() {
    // console.log(window.parent)

    console.log(window.location.href);

    // '?state=S003+Xx03540226#12345&login=true&scope=openid'

    const state = this.getUrlString("state");
    // console.log(state);

    const code = this.getUrlString("code");

    // console.log("code", code);

    if (state) {
      const systemno = state.split("+")[0];

      const password = state.split("+")[1];

      // console.log(systemno);
      // console.log(password);

      this.setState(
        {
          systemno: systemno,
          password: password,
          code: code,
        },
        () => {
          this.isUpdateSucess();
        }
      );
    }
  }

  isUpdateSucess() {
    const { user } = this.props;
    const params = {
      phone: user.phone,
    };
    api.isUpdateSucess({ data: params }).then((result) => {
      if (result.code === 0) {
        const data = result.data;
        if (data.isSyn === 1) {
          // 已激活/密码已修改
          console.log("已激活");
          this.gotData();
        } else {
          //未激活/退出
          console.log("未激活");
          this.loginOutCheck();
        }
      } else {
        message.error(result.msg);
      }
    });
  }

  loginOutCheck() {
    const { user } = this.props;
    const params = {
      phone: user.phone,
      code: this.state.code,
    };
    api.loginOutCheck({ data: params }).then((result) => {
      if (result.code === 0) {
        console.log("退出成功");
        if (window.sessionStorage.getItem("shijilinkurl")) {
          const shijilinkurl = window.sessionStorage.getItem("shijilinkurl");
          // console.log('激活url'+ shijilinkurl);
          window.sessionStorage.setItem("shijiactivate", 1);
          window.parent.document.getElementById("iframeId").src = shijilinkurl;
        } else {
          alert("再次激活url不存在");
          window.parent.location.reload();
        }
      } else {
        alert("退出失败");
        window.parent.location.reload();
      }
    });
  }

  iframeLoad() {
    
    console.log("iframeLoad2");
    console.log("次数2" + this.iframeloadcount);

    const { username, password, shijisrc } = this.state;

    if (this.iframeloadcount === 0) {
      
      console.log("iframeloadfirst2");

      const json = {
        username: username,
        password: password,
        submitForm: "yes",
      };

      // console.log(this.iframe.current);

      this.iframe.current.contentWindow.postMessage(JSON.stringify(json), "*");

      let passwordConfirm = "";
      const urlstr = this.getQueryString(shijisrc, "state");
      if (urlstr) {
        passwordConfirm = urlstr.split("+")[1];
      }

      // 修改密码
      const json2 = {
        passwordConfirm: passwordConfirm,
        password: passwordConfirm,
        submitForm: "yes",
      };

      var timer = setInterval(() => {
        this.iframe.current.contentWindow.postMessage(
          JSON.stringify(json2),
          "*"
        );
        clearInterval(timer);
      }, 2000);

      this.iframeloadcount++;

      // 点击了石基系统
      // localStorage.setItem('shiji', 'yzlogin');
      
    } else {
      console.log("iframeloadother2");
    }
  }

  getQueryString(url, name) {
    if (url.indexOf("?") > -1) {
      const index = url.indexOf("?");
      var query = decodeURI(url.substring(index + 1));
      var vars = query.split("&");
      for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        if (pair[0] === name) {
          return pair[1];
        }
      }
      return null;
    }
    return null;
  }

  gotData() {
    const { user } = this.props;
    const params = {
      phone: user.phone,
      shiji_pwd: this.state.password,
    };
    api.shijisynpwd({ data: params }).then((result) => {
      if (result.code === 0) {
        // this.gotSystemno();
        this.gotSystemDetail();
      } else {
        message.error(result.msg);
      }
    });
  }

  gotSystemDetail() {
    const params = {
      systemno: this.state.systemno,
    };
    api.systemDetail({ data: params }).then((result) => {
      if (result.code === 0) {
        const data = result.data;
        if (data.linkurl) {
          window.open(data.linkurl);
        } else {
          alert("没有配置链接地址，请联系管理员");
        }
        window.parent.location.reload();
      } else {
        message.error(result.msg);
      }
    });
  }

  getUrlString(name) {
    if (window.location.search) {
      var query = decodeURI(window.location.search.substring(1));
      var vars = query.split("&");
      for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        if (pair[0] === name) {
          return pair[1];
        }
      }
      return null;
    }
    return null;
  }

  goBack() {
    window.parent.location.reload();
  }

  render() {
    const { shijisrc } = this.state;
    return (
      <div className="shiji-wrapper">
        <div className="shiji-box">
          <Card title="石基系统自动登录">
            <div className="shiji-content">
              <div className="content-div">系统登录中...</div>
              <div className="content-div">
                <Button
                  type="primary"
                  size="small"
                  onClick={this.goBack.bind(this)}
                >
                  返回
                </Button>
              </div>
            </div>

            {/* iframe */}
            {/* {shijisrc && (
              <div className="fixed-box">
                <iframe
                  title=" "
                  src={shijisrc}
                  onLoad={this.iframeLoad.bind(this)}
                  width="200px"
                  frameBorder="0"
                  height="200px"
                  ref={this.iframe}
                ></iframe>
              </div>
            )} */}
            
          </Card>
        </div>
      </div>
    );
  }
}

export default ShiJi;
