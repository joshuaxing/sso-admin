/*
 * @Author: your name
 * @Date: 2020-09-15 10:54:56
 * @LastEditTime: 2020-09-21 09:39:51
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \my-app\src\App.js
 */
import React, { Component } from "react";
import { Router } from "react-router-dom";
import { Switch, Route, Redirect, browserHistory } from "react-router";
import './App.scss';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from "@/store/actions";
import { Button } from 'antd';

@connect(
  state => state.user,
  dispatch => bindActionCreators(actions, dispatch)
)

class App extends Component {

  handleClick () {
    this.props.Login({
      userId: 66,
      token: ''
    })
  }

  render() {
    
    return (
      <div className="App">
        <Button type="primary" onClick={this.handleClick.bind(this)}>按钮</Button>
        用户ID: {this.props.user.userId}
      </div>
    );
  }
}

/*
function mapStateToProps(state, props) {
  console.log(state)
  return {
    user: state.user.user
  }
}

const increaseAction = { type: 'Login', payload: {
  userId: 1,
  token: ''
}}

function mapDispatchToProps(dispatch) {
  return {
    onIncreaseClick: () => dispatch(increaseAction)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
*/
export default App
