/*
 * @Author: your name
 * @Date: 2020-09-15 10:54:56
 * @LastEditTime: 2020-09-28 16:20:38
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \my-app\src\App.js
 */
import React, { Component } from "react";
import { Switch, Router, Route, Redirect } from "react-router-dom";

import "./App.scss";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from "@/store/actions";
import { routesdefault, routesadmin, history } from "@/router";
import { gotUserToken } from "@/utils/gotToken";

const getRouterByRoutes = (value = []) => {
  let routesArr = [...value, ...routesdefault];
  let renderedRoutesList = [];
  const renderRoutes = (routes, parentPath) => {
    Array.isArray(routes) &&
      routes.forEach((route) => {
        const { path, redirect, children, layout, component } = route;
        if (redirect) {
          renderedRoutesList.push(
            <Redirect
              key={`${parentPath}${path}`}
              exact
              from={path}
              to={`${parentPath}${redirect}`}
            />
          );
        }
        if (component) {
          renderedRoutesList.push(
            layout ? (
              <Route
                key={`${parentPath}${path}`}
                exact
                path={`${parentPath}${path}`}
                render={(props) =>
                  React.createElement(
                    layout,
                    props,
                    React.createElement(component, props)
                  )
                }
              />
            ) : (
              <Route
                key={`${parentPath}${path}`}
                exact
                path={`${parentPath}${path}`}
                component={component}
              />
            )
          );
        }
        if (Array.isArray(children) && children.length > 0) {
          renderRoutes(children, path);
        }
      });
  };

  renderRoutes(routesArr, "");

  return renderedRoutesList;
};

@connect(
  (state) => state.user,
  (dispatch) => bindActionCreators(actions, dispatch)
)
class App extends Component {
  componentDidMount() {
    if (gotUserToken()) {
      const value = JSON.parse(gotUserToken());
      this.props.Login(value);
    } else {
      history.replace('/login')
    }
  }
  render() {
    return (
      <Router history={history}>
        <Switch>
          {gotUserToken()
            ? getRouterByRoutes(routesadmin)
            : getRouterByRoutes()}
        </Switch>
      </Router>
    );
  }
}

export default App;
