/*
 * @Author: your name
 * @Date: 2020-09-23 15:47:41
 * @LastEditTime: 2020-12-30 11:01:24
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \sso-admin\src\components\HeaderBar\index.js
 */
import React, { Component, useState, useEffect } from "react";
import { withRouter } from "react-router-dom";
import "./style.scss";
import * as api from "@/api/user";
import { Dropdown, Menu, Row, Col, message, Modal, Form, Input } from "antd";
import screenfull from "screenfull";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from "@/store/actions";
import {
  UserOutlined,
  LogoutOutlined,
  ArrowsAltOutlined,
  ShrinkOutlined,
  PhoneOutlined,
  LockOutlined,
} from "@ant-design/icons";
import { clearUserToken } from "@/utils/gotToken";
import avatar from "./img/avatar.jpg";

const PasswordCom = ({ callback, initvisible, props}) => {

  const [form] = Form.useForm();
  const [visible, setVisible] = useState(initvisible);
  const { user, Logout, history} = props

  useEffect(() => {
    // Update the document title using the browser API
    setVisible(initvisible);
  });
  
  const hanldeEdit = (values) => {
    const params = {
      phone: user.phone,
      confirmPassword: values.surepassword,
      newPassword: values.password,
      password: values.oldpassword
    };
    api.modifyPwd({ data: params }).then((result) => {
      if (result.code === 0) {
        //修改成功
        // form.resetFields();
        // callback();
         //清除登录态
         message.success('密码修改成功');
         clearUserToken();
         Logout();
         history.replace("/login");
      } else {
        message.error(result.msg);
      }
    });
  };

  const handleOk = () => {
    form.validateFields()
    .then(values => {
      const {oldpassword, password, surepassword} = values
      if (password !== surepassword) {
        message.error('新密码和确认密码不一致');
      } else if (!/(^\d+$)|(^[a-zA-Z]+$)|(^[0-9A-Za-z]+$)/.test(password)){
        message.error('新密码必须有数字、字母、数字字母组合');
      } else {
        hanldeEdit(values)
      }
    })
    .catch(errorInfo => {
      console.log(errorInfo)
    })
  };



  const handleCancel = () => {
    form.resetFields();
    callback();
  };

  return (
    <Modal
      title="修改密码"
      visible={visible}
      onOk={handleOk}
      onCancel={handleCancel}
    >
      <Form
        form={form}
        name="form-hooks"
      >
        <Form.Item
          label="旧密码"
          name="oldpassword"
          rules={[{ required: true, message: "请输入..." }]}
        >
          <Input.Password />
        </Form.Item>
        <Form.Item
          label="新密码"
          name="password"
          rules={[{ required: true, message: "请输入..." }]}
        >
          <Input.Password />
        </Form.Item>
        <Form.Item
          label="确认密码"
          name="surepassword"
          rules={[{ required: true, message: "请输入..." }]}
        >
          <Input.Password />
        </Form.Item>
      </Form>
    </Modal>
  );
};

@connect(
  (state) => state.user,
  (dispatch) => bindActionCreators(actions, dispatch)
)
class HeaderBar extends Component {
  state = {
    isFullscreen: 1,
    visible: false,
  };

  componentDidMount() {
    screenfull.onchange(() => {
      const isFullscreen = screenfull.isFullscreen ? 2 : 1;
      this.setState({
        isFullscreen: isFullscreen,
      });
    });
  }

  componentWillUnmount() {
    screenfull.off("change");
  }

  toggle = () => {
    this.props.onToggle();
  };

  screenfullToggle = () => {
    if (screenfull.enabled) {
      screenfull.toggle();
    }
  };

  logout = () => {
    const params = {};
    api.loginout({ data: params }).then((result) => {
      if (result.code === 0) {
        //清除登录态
        clearUserToken();
        this.props.Logout();
        this.props.history.replace("/login");
      } else {
        message.error(result.msg);
      }
    });

    // 退出石基登录
    
    // if (localStorage.getItem('shiji')) {
    //   this.shijiLoginOut();
    // }
     
  };

  shijiLoginOut () {
    const { user } = this.props;
    const params = {
      phone: user.phone
    }
    api.shijiLoginOut({ data: params }).then((result) => {
      if (result.code === 0) {
        console.log('退出石基登录');
      } else {
        message.error(result.msg);
      }
    });
  }

  handleEdit = () => {
    this.setState({
      visible: true
    });
  };

  handleClose = () => {
    this.setState({
      visible: false
    });
  };

  render() {
    const { user } = this.props;
    const menu = (
      <Menu className="menu">
        <Menu.Item>
          <PhoneOutlined />
          <span>{user.phone}</span>
        </Menu.Item>
        <Menu.Item>
          <LockOutlined />
          <span onClick={this.handleEdit}>修改密码</span>
        </Menu.Item>
        <Menu.Item>
          <LogoutOutlined />
          <span onClick={this.logout}>退出登录</span>
        </Menu.Item>
      </Menu>
    );

    return (
      <div id="headerbar">
        <Row>
          <Col span={12}></Col>
          <Col span={12}>
            <div style={{ lineHeight: "64px", float: "right" }}>
              <ul className="header-ul">
                <li>
                  <Dropdown overlay={menu}>
                    <div>
                      <span style={{ marginRight: "10px" }}>
                        您好，{user.truename}
                      </span>
                      <img src={avatar} alt="" />
                    </div>
                  </Dropdown>
                </li>
              </ul>
            </div>
          </Col>
        </Row>
        <PasswordCom
          callback={this.handleClose}
          initvisible={this.state.visible}
          props={this.props}
        />
      </div>
    );
  }
}

export default withRouter(HeaderBar);
