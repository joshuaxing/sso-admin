/*
 * @Author: your name
 * @Date: 2020-03-03 11:19:50
 * @LastEditTime: 2020-09-15 17:33:56
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \yuanzhou-salesH5\src\store\reducer.js
 */
const initialState = {
  result: [],
};

export default function product(state = initialState, action) {
  const { type, payload } = action;
  switch (type) {
    case "Add":
      return {
        ...state,
        result: payload,
      };
    default:
      return state;
  }
}
