/*
 * @Author: your name
 * @Date: 2020-03-03 11:19:50
 * @LastEditTime: 2020-11-12 10:44:57
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \yuanzhou-salesH5\src\store\reducer.js
 */
const initialState = {
  user: {
    userId: 0,
    token: '',
    truename: '',
    jobnum: '',
    phone: ''
  }
};

export default function login(state = initialState, action) {
  const { type, payload } = action;
  switch (type) {
    case 'Login':
      return {
        ...state,
        user: payload
      }
    default:
      return state;
  }
}
